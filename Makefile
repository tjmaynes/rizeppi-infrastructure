APPLICATION_NAME=rizeppi

start_services:
	./scripts/start-services.sh

teardown:
	 ./scripts/teardown.sh

deploy:
	 ./scripts/deploy.sh

logs:
	 ./scripts/log-stream.sh "$(SERVICE_NAME)"

refresh:
	curl -i -X POST -H 'Content-Type: application/json' -d {} http://localhost:$(GATEWAY_SERVICE_PORT)/actuator/refresh

debug_networking:
	kubectl run -it --rm --restart=Never busybox --image=gcr.io/google-containers/busybox sh
