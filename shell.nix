with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "rizeppi-infrastructure";
  buildInputs = [
    azure-cli
    jq
    tilt
  ];
}
