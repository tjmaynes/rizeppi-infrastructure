# Rizeppi Infrastructure
[![CI](https://github.com/tjmaynes/rizeppi-infrastructure/actions/workflows/ci.yml/badge.svg)](https://github.com/tjmaynes/rizeppi-infrastructure/actions/workflows/ci.yml)
> A gitops repository for a recipe sharing application.

## Requirements

- [GNU Make](https://www.gnu.org/software/make)
- [Docker](https://docs.docker.com/docker-for-mac/install/)
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
- [Tilt](https://tilt.dev/)
- [jq](https://stedolan.github.io/jq/)

## Usage

To deploy the services, run the following command:
```bash
make deploy
```

To ship the project, run the following command:
```bash
make ship_it
```
