#!/bin/bash

set -e

function check_requirements() {
  if [[ -z "$(command -v az)" ]]; then
    echo "Please install the azure-cli tool for creating your azure spring cloud instance"
    exit 1
  elif [[ -z "$AZURE_RESOURCE_GROUP" ]]; then
    echo "Please set the environment variable for 'AZURE_RESOURCE_GROUP' for deploying your azure spring cloud apps"
    exit 1
  elif [[ "$(az group exists --name $AZURE_RESOURCE_GROUP)" == "false" ]]; then
    echo "Please create a resource group for '$AZURE_RESOURCE_GROUP' before running this script"
    exit
  elif [[ -z "$APPLICATION_NAME" ]]; then
    echo "Please set the environment variable for 'APPLICATION_NAME' for deploying your azure spring cloud apps"
    exit 1
  fi
}

function setup_azure_cli() {
  if  [[ -z  "$(az extension list | jq '.[] | select(.name == "spring-cloud")')" ]]; then
    az extension add --name spring-cloud --yes
  fi

  az configure --defaults \
    group=$AZURE_RESOURCE_GROUP \
    spring-cloud=$APPLICATION_NAME
}

function main() {
  check_requirements

  setup_azure_cli

  az group delete --yes
}

main