#!/usr/bin/env bash

set -e

function create_and_set_namespace() {
  NAMESPACE_NAME=$1

  if [[ -z "$(kubectl get namespaces -o json | jq -c --arg name "$NAMESPACE_NAME" '.items[] | select(.metadata.name | contains($name)) | .metadata.name')" ]]; then
    kubectl create namespace "$NAMESPACE_NAME"
  fi

  kubectl config set-context --current --namespace="$NAMESPACE_NAME"
}

function main() {
  create_and_set_namespace "development"

  pushd k8s || exit 1
  skaffold dev --cache-artifacts=true
  popd || exit 1
}

main