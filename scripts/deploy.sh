#!/bin/bash

set -e

function check_requirements() {
  if [[ -z "$(command -v az)" ]]; then
    echo "Please install the azure-cli tool for creating your azure spring cloud instance"
    exit 1
  elif [[ -z "$(command -v jq)" ]]; then
    echo "Please install the jq tool for reading your azure cli json info"
    exit 1
  elif [[ -z "$AZURE_RESOURCE_GROUP" ]]; then
    echo "Please set the environment variable for 'AZURE_RESOURCE_GROUP' for deploying your azure spring cloud apps"
    exit 1
  elif [[ -z "$APPLICATION_NAME" ]]; then
    echo "Please set the environment variable for 'APPLICATION_NAME' for deploying your azure spring cloud apps"
    exit 1
  elif [[ -z "$VERSION" ]]; then
    echo "Please set the environment variable for 'VERSION' for deploying your azure spring cloud apps"
    exit 1
  elif [[ -z "$RECIPE_DB_URL" ]]; then
    echo "Please set the environment variable for 'RECIPE_DB_URL' for creating and connecting to your database in azure"
    exit 1
  elif [[ -z "$SERVICES_LOCATION" ]]; then
    echo "Please provide a location for the services directory: SERVICES_LOCATION"
    exit 1
  elif [[ ! -d "$SERVICES_DIRECTORY" ]]; then
    echo "Please provide a real location for the services directory: ${SERVICES_LOCATION}"
    exit 1
  fi
}

function setup_azure_cli() {
  if  [[ -z  "$(az extension list | jq '.[] | select(.name == "spring-cloud")')" ]]; then
    az extension add --name spring-cloud --yes
  fi

  az configure --defaults \
    group=$AZURE_RESOURCE_GROUP \
    spring-cloud=$APPLICATION_NAME
}

function create_resource_group() {
  if [[ "$(az group exists --name $AZURE_RESOURCE_GROUP)" == "false" ]]; then
    az group create --name "$AZURE_RESOURCE_GROUP"
  fi
}

function create_azure_spring_cloud_instance() {
  if [[ -z "$(az spring-cloud list | jq --raw-output --arg APPLICATION_NAME "$APPLICATION_NAME" '.[] | select(.name == $APPLICATION_NAME)')" ]]; then
    az spring-cloud create --name "$APPLICATION_NAME" --location "eastus" --output none
  fi
}

function create_postgresql_database() {
  if [[ -z "$(az postgres server list | jq '.name')" ]]; then
    az postgres server create \
      --name "$APPLICATION_NAME" \
      --admin-user "$DB_USER" \
      --admin-password "$DB_PASSWORD" \
      --sku-name "GP_Gen4_2"
  fi

  if [[ -z "$(az postgres db list | jq '.name')" ]]; then
    az postgres db create \
      --server-name "$APPLICATION_NAME" \
      --name "$APPLICATION_NAME" \
      --charset "UTF8"
  fi
}

function setup_config_server() {
  if [[ -z "$(az spring-cloud config-server show --name "$APPLICATION_NAME" | jq '.name')" ]]; then
    az spring-cloud \
      config-server git set \
      --name "$APPLICATION_NAME" \
      --uri https://github.com/tjmaynes/rizeppi-config
  fi
}

function test_app() {
  SERVICE_NAME=$1

  pushd "$SERVICES_LOCATION" || exit 1
  ./gradlew ":services:${SERVICE_NAME}:test"
  popd
}

function build_app() {
  SERVICE_NAME=$1; VERSION=$2

  pushd "$SERVICES_LOCATION" || exit 1
  ./gradlew clean ":services:${SERVICE_NAME}:build" -x test -Pversion=$VERSION -Dspring.profiles.active=production
  popd || exit 1
}

function deploy_app() {
  SERVICE_NAME=$1; VERSION=$2; ASSIGN_ENDPOINT=$3; ENV_OPTS=${@:4}

  if [[ -z "$(az spring-cloud app list | jq --raw-output --arg SERVICE_NAME "$SERVICE_NAME" '.[] | select(.name == $SERVICE_NAME)')" ]]; then
    ARGS="--name $SERVICE_NAME --instance-count 1 --memory 2 --runtime-version Java_11"
    if [[ "$ASSIGN_ENDPOINT" == "true" ]]; then
      ARGS="$ARGS --assign-endpoint"
    fi

    az spring-cloud app create $ARGS
  fi

  az spring-cloud app deploy \
    --name "$SERVICE_NAME" \
    --jar-path "services/$SERVICE_NAME/build/libs/$SERVICE_NAME-$VERSION.jar" \
    --jvm-options="-Xms2048m -Xmx2048m" \
    --env \
      CONFIG_SERVICE_URI="$BUILD_IN_SPRING_CLOUD_CONFIG_URI" \
      DISCOVERY_SERVICE_URI="$BUILD_IN_EUREKA_CLIENT_SERVICEURL_DEFAULTZONE" \
      $ENV_OPTS
}

function test_build_and_deploy_app() {
  SERVICE_NAME=$1; VERSION=$2; ASSIGN_ENDPOINT=$3; ENV_OPTS="${@:4}"

#  test_app "$SERVICE_NAME"
  build_app "$SERVICE_NAME" "$VERSION"
  deploy_app "$SERVICE_NAME" "$VERSION" "$ASSIGN_ENDPOINT" "$ENV_OPTS"
}

function main() {
  check_requirements

  setup_azure_cli
  create_resource_group
  create_azure_spring_cloud_instance
  create_postgresql_database
  setup_config_server

  test_build_and_deploy_app "gateway-service" "$VERSION" "true"
  test_build_and_deploy_app "recipe-service" "$VERSION" "false" RECIPE_DB_URL="$RECIPE_DB_URL"

  az spring-cloud app list -o table
}

main
